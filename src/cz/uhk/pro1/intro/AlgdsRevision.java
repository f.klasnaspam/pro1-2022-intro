package cz.uhk.pro1.intro;

public class AlgdsRevision {

    public static void main(String[] args) {

        /* Následující pole obsahuje bodové zisky studentů ze zápočtového testu. Pro získání zápočtu bylo nutné napsat
        test alespoň na 60 % bodů, maximum bylo 10 bodů. Kolik studentů získalo zápočet? */
        float[] scores = {8, 5, 3.5f, 6, 10, 7, 6.5f, 2, 9, 8};
        float minToPass = 0.6f * 10;

        int numOfPassed = 0;
        for (int i = 0; i < scores.length; i++) {
            if (scores[i] >= minToPass) {
                numOfPassed++;
            }
        }

        System.out.println("Zápočet získalo " + numOfPassed + " studentů.");

    }

}
